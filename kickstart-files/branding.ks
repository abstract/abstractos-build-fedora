%post --log=/mnt/sysimage/root/ks-post.log

# Change fedora logos & anaconda logos
#cp -fr /home/user/Downloads/livecd/root-brynux/usr/share/icons/brynux/* $INSTALL_ROOT/usr/share/icons/Fedora
#cp -fr /home/user/Downloads/livecd/root-brynux/* $INSTALL_ROOT/
#chmod -R 755 $INSTALL_ROOT/usr/share

# Copy initrd with brynux boot screen to livecd root
#echo "Install Root is $INSTALL_ROOT"
#echo "Live Root is $LIVE_ROOT"
#cp $INSTALL_ROOT/boot/initrd-plymouth.img $LIVE_ROOT/isolinux/

#cp -fr /home/user/Downloads/livecd/root-brynux/.VolumeIcon.icns $LIVE_ROOT



echo "Abstract OS 0.1" > /etc/fedora-release
echo "Abstract OS 0.1" > /etc/redhat-release
echo "Abstract OS 0.1" > /etc/system-release
echo "cpe:/o:abstractos:abstractos:24" > /etc/system-release-cpe
echo "AbstractOS Kernel \r on an \m (\l)" > /etc/issue
echo "AbstractOS Kernel \r on an \m (\l)" > /etc/issue.net

cat >/etc/os-release << EOF
NAME="Abstract OS"
VERSION="0.0.1"
ID=abstractos
VERSION_ID=0.0.1
PRETTY_NAME="Abstract OS 0.1"
ANSI_COLOR="0;34"
CPE_NAME="cpe:/o:abstractos:abstractos:35"
HOME_URL="https://www.abstractos.org/"
BUG_REPORT_URL="https://gitlab.com/abstract/"
REDHAT_BUGZILLA_PRODUCT="Fedora"
REDHAT_BUGZILLA_PRODUCT_VERSION=35
REDHAT_SUPPORT_PRODUCT="Fedora"
REDHAT_SUPPORT_PRODUCT_VERSION=35
VARIANT="Desktop Edition"
VARIANT_ID=desktop
EOF

%end
