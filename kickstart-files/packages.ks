# ---
# Repos
# ---

url --mirrorlist="https://mirrors.fedoraproject.org/mirrorlist?repo=rawhide&arch=$basearch"
repo --name="rawhide" --mirrorlist=https://mirrors.fedoraproject.org/mirrorlist?repo=rawhide&arch=$basearch
repo --name="fedora" --mirrorlist=http://mirrors.fedoraproject.org/mirrorlist?repo=fedora-$releasever&arch=$basearch
repo --name="rpmfusion-free" --mirrorlist=http://mirrors.rpmfusion.org/mirrorlist?repo=free-fedora-$releasever&arch=$basearch
repo --name="rpmfusion-non-free" --mirrorlist=http://mirrors.rpmfusion.org/mirrorlist?repo=nonfree-fedora-$releasever&arch=$basearch


%packages
# ---
# Groups
# ---

@anaconda-tools
@base-x
@core
@firefox
@fonts
@gnome-desktop
@guest-desktop-agents
@hardware-support
@multimedia
@networkmanager-submodules
@printing
@workstation-product

# ---
# Packages
# ---

aajohan-comfortaa-fonts
anaconda
anaconda-install-env-deps
anaconda-live
chkconfig
dracut-live
firewalld
gedit
generic-logos
git
glibc-all-langpacks
gnome-packagekit
initscripts
kernel
kernel-modules
kernel-modules-extra
libvirt
memtest86+
nano
python3-firewall
syslinux
tar
virt-manager
wget

# ---
# Removals
# ---

-@dial-up
-@input-methods
-@standard
-gfs2-utils
-java-1.8.0-openjdk-headless
-rpmfusion-*
-unoconv
%end
